
package com.galvanize;

//import jdk.vm.ci.code.site.Call;

public class CallingCard {
    int rate;
    int minutes;
    public CallingCard(int rate){
        this.rate = rate;
    }
    public void addDollars(int dollars){
        minutes = minutes + ((dollars*100)/rate);
    }

    public int getRemainingMinutes(){
        return minutes;
    }
    public void useMinutes(int used){
        minutes = minutes - used;
        if(minutes<0)
            minutes = 0;
    }
    public int getRate() {
        return rate;
    }
}
