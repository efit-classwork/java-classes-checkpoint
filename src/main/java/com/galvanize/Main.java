package com.galvanize;

//import jdk.vm.ci.code.site.Call;

public class Main {
    public static void main(String[] args){
        CallingCard card = new CallingCard(20);
        card.addDollars(1);
        CellPhone phone = new CellPhone(card);
        phone.call("555-1111");
        phone.tick();
        phone.tick();
        phone.endCall();
        phone.call("555-3333");
        phone.tick();
        phone.tick();
        phone.tick();
        phone.getHistory();
    }
}
