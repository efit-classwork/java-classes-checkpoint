
package com.galvanize;

public class CellPhone {
    boolean active = false;
    CallingCard card;
    String number;
    String callHistory;
    int callLength;
    public CellPhone(CallingCard card){
        this.card = card;
    }
    public void call(String number){
        if(!active){
            active = true;
            this.number = number;
            callLength = 0;

        }else{
            System.out.println("Line already in use.");
        }
    }
    public void endCall(){
        if(callHistory == null){
            callHistory = number + " (" + lengthGrammar() + ")";
        }else {
            callHistory = callHistory + ", " + number + " (" + lengthGrammar() + ")";
        }
        active = false;
    }
    public boolean isTalking(){
        boolean check = false;
        if(active)
            check = true;
        return check;
    }
    public void tick(){
        card.useMinutes(1);
        callLength++;
        if(card.getRemainingMinutes() == 0) {
            endCall();
            callHistory = callHistory + " (cut off at " + lengthGrammar() + ")";
        }
    }
    public String lengthGrammar(){
        String lengthString;
        if(callLength == 1){
            lengthString = callLength + " minute";
        }else{
            lengthString = callLength + " minutes";
        }
        return lengthString;
    }
    public void getHistory(){
        System.out.println(callHistory);
    }
}
